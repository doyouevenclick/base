def cpuinfo():
    cpuinfo = {}

    with open('/proc/cpuinfo') as cpuinfo_file:
        for line in cpuinfo_file:
            if not line.strip():
                break

            key, value = map(str.strip, line.split(':', 1))
            if key == 'cpu MHz':
                # This is the current frequency, which will change over time
                continue

            if key in ('processor', 'physical_id', 'core_id',
                       'initial_apicid', 'acpid'):
                # Skipping core-specific information
                continue

            # Some field-specific parsing
            if key in ('bugs', 'flags'):
                value = value.split()

            if key in ('fpu', 'fpu_exception', 'wp'):
                try:
                    value = {'yes': True, 'no': False}[value]
                except KeyError:
                    continue

            cpuinfo[key.lower().replace(' ', '_')] = value

    return {'cpuinfo': cpuinfo}
