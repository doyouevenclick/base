certbot: pkg.installed

python-boto: pkg.installed
python3-boto: pkg.installed
python-boto3: pkg.installed
python3-boto3: pkg.installed


"/usr/bin/certbot register --agree-tos --noninteractive --email webmaster@astro73.com":
  cmd.run:
    - onchanges:
      - pkg: certbot

{% if grains['pythonversion'][0] == 2 %}
{% set pipbin = "/usr/bin/pip" %}
{% else %}
{% set pipbin = "/usr/bin/pip3" %}
{% endif %}

spirofs:
  pip.installed:
    - bin_env: {{pipbin}}
    # watch_in: service: salt-master

salt.doyoueven.click:
  acme.cert:
    - email: webmaster@astro73.com

CherryPy:
  pip.installed:
    - bin_env: {{pipbin}}

salt-api:
  pkg.installed: []
  service.running:
    - enable: true
    - requires:
      - pkg: salt-api
      - pip: CherryPy
    - watch:
      - acme: salt.doyoueven.click
      - pip: CherryPy

cert-renew:
 schedule.present:
   - function: state.apply
   - job_args:
     - master
   - hours: 12
   - splay: 300
